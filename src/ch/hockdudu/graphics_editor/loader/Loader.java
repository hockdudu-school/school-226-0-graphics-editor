package ch.hockdudu.graphics_editor.loader;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.component.FormFactory;
import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class Loader implements LoaderInterface {

    @Override
    public List<Class<? extends Form>> getForms() {
        Reflections reflections = new Reflections("ch.hockdudu.graphics_editor.component.forms");
        Set<Class<? extends Form>> forms = reflections.getSubTypesOf(Form.class);

        ArrayList<Class<? extends Form>> formsList = new ArrayList<>(forms);

        formsList.sort(Comparator.comparing(Class::getSimpleName));

        return formsList;
    }

    @Override
    public FormFactory getFormFactoryFromClass(Class<? extends Form> class1) {
        try {
            Class factory = Class.forName(String.format("ch.hockdudu.graphics_editor.component.forms.factories.%sFactory", class1.getSimpleName()));

            if (FormFactory.class.isAssignableFrom(factory)) {
                //noinspection unchecked
                Constructor<FormFactory> factoryConstructor = factory.getConstructor();
                return factoryConstructor.newInstance();
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Class<? extends Form> formClassFromName(String className) {
        try {
            Class formClass = Class.forName("ch.hockdudu.graphics_editor.component.forms." + className);

            if (Form.class.isAssignableFrom(formClass)) {
                //noinspection unchecked
                return (Class<? extends Form>) formClass;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}
