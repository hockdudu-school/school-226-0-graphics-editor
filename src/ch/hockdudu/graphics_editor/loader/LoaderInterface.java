package ch.hockdudu.graphics_editor.loader;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.component.FormFactory;

import java.util.List;

public interface LoaderInterface {
    List<Class<? extends Form>> getForms();

    FormFactory getFormFactoryFromClass(Class<? extends Form> class1);

    Class<? extends Form> formClassFromName(String className);
}
