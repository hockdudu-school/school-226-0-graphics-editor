package ch.hockdudu.graphics_editor.display;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.editor.Editor;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class Infobar extends JPanel implements InfobarInterface {

    private JLabel modeLabel;
    private JLabel mouseCoords;

    private Editor.Mode mode;
    private String form;

    public Infobar() {
        setLayout(new GridLayout(0, 2));
        setBorder(new EmptyBorder(5, 5, 5, 5));

        modeLabel = new JLabel("");
        modeLabel.setHorizontalAlignment(SwingConstants.LEFT);
        add(modeLabel);

        mouseCoords = new JLabel("");
        mouseCoords.setHorizontalAlignment(SwingConstants.LEFT);
        add(mouseCoords);
    }

    public void updateMode(Editor.Mode mode) {
        this.mode = mode;
        updateFormAndModeLabel();
    }

    public void updateForm(Class<? extends Form> form) {
        this.form = form.getSimpleName();
        updateFormAndModeLabel();
    }

    private void updateFormAndModeLabel() {
        if (mode == null || form == null) return;

        String text = "";
        switch (mode) {
            case movement:
                text = "Selection mode";
                break;
            case creation:
                text = String.format("Creation mode: %s", form);
                break;
            case rotation:
                text = "Rotation mode";
                break;
            case deletion:
                text = "Deletion mode";
                break;
        }
        this.modeLabel.setText(text);
    }

    public void updateMouseCoords(Point mouseCoords) {
        this.mouseCoords.setText(String.format("Coordinates: x: %d, y: %d", mouseCoords.x, mouseCoords.y));
    }
}
