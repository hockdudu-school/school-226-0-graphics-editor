package ch.hockdudu.graphics_editor.display;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.editor.Editor;

import java.awt.*;

public interface InfobarInterface {
    void updateMode(Editor.Mode mode);
    void updateForm(Class<? extends Form> form);
    void updateMouseCoords(Point mouseCoords);
}
