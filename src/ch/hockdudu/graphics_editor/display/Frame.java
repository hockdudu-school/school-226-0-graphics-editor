package ch.hockdudu.graphics_editor.display;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.editor.Editor;
import ch.hockdudu.graphics_editor.component.Group;
import ch.hockdudu.graphics_editor.loader.Loader;
import ch.hockdudu.graphics_editor.loader.LoaderInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.List;

public class Frame extends JFrame {

    private Canvas canvas;
    private Editor editor;
    private Infobar infobar;
    private Loader loader;

    public Frame(String title) {
        setTitle(title);

        setLayout(new GridBagLayout());

        GridBagConstraints constraints = new GridBagConstraints();

        canvas = new Canvas();
        canvas.setBackground(Color.WHITE);
        infobar = new Infobar();
        loader = new Loader();
        editor = new Editor(canvas, infobar, loader);

        prepareMenubar();

        Component toolbar = getModeToolbar(editor);
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridy = 0;
        constraints.weightx = 1;
        constraints.weighty = 0;
        add(toolbar, constraints);

        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridy = 1;
        constraints.weightx = 1;
        constraints.weighty = 1;
        add(canvas, constraints);

        Component objects = getObjectsToolbar(editor);
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridy = 2;
        constraints.weightx = 1;
        constraints.weighty = 0;
        add(objects, constraints);

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridy = 3;
        constraints.weightx = 1;
        constraints.weighty = 0;
        add(infobar, constraints);

        prepareGUI();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
    }

    private void prepareMenubar() {
        var menubar = new JMenuBar();

        var fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);

        var saveMenuItem = new JMenuItem("Save");
        saveMenuItem.setMnemonic(KeyEvent.VK_S);
        saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
        saveMenuItem.setToolTipText("Save canvas to file");
        saveMenuItem.addActionListener((event) -> saveToFile());

        var openMenuItem = new JMenuItem("Open");
        openMenuItem.setMnemonic(KeyEvent.VK_O);
        openMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
        openMenuItem.setToolTipText("Open canvas from file");
        openMenuItem.addActionListener((event) -> loadFromFile());

        var exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.setMnemonic(KeyEvent.VK_X);
        exitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK));
        exitMenuItem.setToolTipText("Exit application");
        exitMenuItem.addActionListener((event) -> System.exit(0));


        fileMenu.add(saveMenuItem);
        fileMenu.add(openMenuItem);
        fileMenu.addSeparator();
        fileMenu.add(exitMenuItem);

        menubar.add(fileMenu);

        setJMenuBar(menubar);
    }

    private Component getModeToolbar(ActionListener listener) {
        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);

        JButton selectionModeButton = new JButton("Movement");
        selectionModeButton.setActionCommand("mode." + Editor.Mode.movement.name());
        selectionModeButton.setMnemonic(KeyEvent.VK_S);
        selectionModeButton.addActionListener(listener);

        JButton createObjectModeButton = new JButton("Creation");
        createObjectModeButton.setActionCommand("mode." + Editor.Mode.creation.name());
        createObjectModeButton.setMnemonic(KeyEvent.VK_C);
        createObjectModeButton.addActionListener(listener);

        JButton rotationModeButton = new JButton("Rotation");
        rotationModeButton.setActionCommand("mode." + Editor.Mode.rotation.name());
        rotationModeButton.setMnemonic(KeyEvent.VK_T);
        rotationModeButton.addActionListener(listener);

        JButton deletionModeButton = new JButton("Deletion");
        deletionModeButton.setActionCommand("mode." + Editor.Mode.deletion.name());
        deletionModeButton.setMnemonic(KeyEvent.VK_D);
        deletionModeButton.addActionListener(listener);

        toolbar.add(selectionModeButton);
        toolbar.add(createObjectModeButton);
        toolbar.add(rotationModeButton);
        toolbar.add(deletionModeButton);

        return toolbar;
    }

    private Component getObjectsToolbar(ActionListener listener) {
        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);

        List<Class<? extends Form>> formClassesList = loader.getForms();

        for (Class<? extends Form> formClass : formClassesList) {
            JButton button = new JButton(formClass.getSimpleName());
            button.setActionCommand("form." + formClass.getSimpleName());
            button.addActionListener(listener);
            toolbar.add(button);
        }

        return toolbar;
    }

    private void prepareGUI() {
        Dimension windowSize = new Dimension(800, 600);
        setSize(windowSize);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int windowPositionX = (screenSize.width - windowSize.width) / 2;
        int windowPositionY = (screenSize.height - windowSize.height) / 2;
        setLocation(windowPositionX, windowPositionY);
    }

    private void saveToFile() {
        JFileChooser chooser = new JFileChooser();
        int returnValue = chooser.showSaveDialog(this);
        if (returnValue != JFileChooser.APPROVE_OPTION) {
            return;
        }

        try {
            FileOutputStream fileOut = new FileOutputStream(chooser.getSelectedFile());
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(canvas.getForms());
            out.close();
            fileOut.close();
            // TODO: Inform user save was successful
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadFromFile() {
        JFileChooser chooser = new JFileChooser();
        int returnValue = chooser.showOpenDialog(this);
        if (returnValue != JFileChooser.APPROVE_OPTION) {
            return;
        }

        try {
            FileInputStream fileIn = new FileInputStream(chooser.getSelectedFile());
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Object deserialized = in.readObject();
            canvas.setForms((Group) deserialized);
            canvas.repaint();

        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            e.printStackTrace();
        }
    }

    public Canvas getCanvas() {
        return canvas;
    }
}
