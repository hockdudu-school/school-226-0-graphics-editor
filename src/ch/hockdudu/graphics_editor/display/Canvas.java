package ch.hockdudu.graphics_editor.display;

import ch.hockdudu.graphics_editor.component.Component;
import ch.hockdudu.graphics_editor.component.Group;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

public class Canvas extends JPanel {
    private final Group forms = new Group();

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

        for (Component form : forms.getForms()) {
            form.draw(graphics2D);
        }
    }

    public void add(Component... forms) {
        this.forms.add(Arrays.asList(forms));
    }

    public void remove(Component... forms) {
        this.forms.remove(Arrays.asList(forms));
    }

    public void clearAll() {
        forms.clear();
    }

    public Group group(Component... forms) {
        Group group = new Group();
        for (Component form : forms) {
            if (this.forms.remove(form)) {
                group.add(form);
            }
        }
        this.forms.add(group);
        return group;
    }

    public void ungroup(Group... groups) {
        for (Group group : groups) {
            this.forms.add(group.getForms());
            this.remove(group);
        }
    }

    public Group getForms() {
        return forms;
    }

    public void setForms(Group forms) {
        this.forms.clear();
        this.forms.add(forms.getForms());
    }
}
