package ch.hockdudu.graphics_editor.editor.mode;

import ch.hockdudu.graphics_editor.display.Canvas;
import ch.hockdudu.graphics_editor.editor.Editor;
import ch.hockdudu.graphics_editor.component.Group;

import java.awt.event.MouseEvent;

public class Rotation extends Selection {

    public Rotation(Editor editor, Canvas canvas, Group forms) {
        super(editor, canvas, forms);
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        if (clickPosition == null) return;

        double sumX = formsToApplyTransform.stream().mapToDouble(form -> (int) form.getX()).reduce(Double::sum).orElse(0);
        double sumY = formsToApplyTransform.stream().mapToDouble(form -> (int) form.getY()).reduce(Double::sum).orElse(0);

        double meanX = sumX / formsToApplyTransform.size();
        double meanY = sumY / formsToApplyTransform.size();

        double xDiffBefore = clickPosition.getX() - meanX;
        double yDiffBefore = clickPosition.getY() - meanY;
        double rotationBefore = Math.atan2(yDiffBefore, xDiffBefore);

        double xDiffAfter =  mouseEvent.getPoint().getX() - meanX;
        double yDiffAfter =  mouseEvent.getPoint().getY() - meanY;
        double rotationAfter = Math.atan2(yDiffAfter, xDiffAfter);

        double rotationDifference = rotationAfter - rotationBefore;


        formsToApplyTransform.forEach(form -> {
            double formDiffToMeanX = form.getX() - meanX;
            double formDiffToMeanY = form.getY() - meanY;

            double formDistanceToMean = Math.sqrt(Math.pow(formDiffToMeanX, 2) + Math.pow(formDiffToMeanY, 2));
            double formRelativeRotationToMean = Math.atan2(formDiffToMeanY, formDiffToMeanX);

            double formNewRelativeRotationToMean = formRelativeRotationToMean + rotationDifference;

            double formNewDiffX = Math.cos(formNewRelativeRotationToMean) * formDistanceToMean;
            double formNewDiffY = Math.sin(formNewRelativeRotationToMean) * formDistanceToMean;

            form.rotate(form.getRotation() + rotationDifference);

            form.setX(meanX + formNewDiffX);
            form.setY(meanY + formNewDiffY);
        });

        canvas.repaint();

        clickPosition = mouseEvent.getPoint();
    }
}
