package ch.hockdudu.graphics_editor.editor.mode;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.display.Canvas;
import ch.hockdudu.graphics_editor.editor.Editor;
import ch.hockdudu.graphics_editor.component.Group;

import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Collections;
import java.util.List;

public abstract class BaseMode implements MouseListener, MouseMotionListener {
    protected Editor editor;
    protected Canvas canvas;
    protected Group forms;

    public BaseMode(Editor editor, Canvas canvas, Group forms) {
        this.editor = editor;
        this.canvas = canvas;
        this.forms = forms;
    }


    protected List<Form> reversedFormsList() {
        // Makes sure that the z-index is looped correctly
        List<Form> reversedForms = forms.getFlatForms().subList(0, forms.getFlatForms().size()); // shallow copy
        Collections.reverse(reversedForms);

        return reversedForms;
    }

    @SuppressWarnings("WeakerAccess")
    protected List<Form> clickedForms(Point click) {
        List<Form> reversedForms = reversedFormsList();

        // Click either inside specific form or, if form is selected selected, inside bounds
        // intersects is used instead of contains because lines always return false for contains
        Form[] clickedFormOptional = reversedForms.stream()
                .filter(form1 -> {
                    Shape rotatedShape = form1.getRotatedShape();
                    return rotatedShape.intersects(click.getX() - 10, click.getY() - 10, 20, 20) || form1.isSelected() && rotatedShape.getBounds2D().contains(click);
                }).toArray(Form[]::new);

        return List.of(clickedFormOptional);
    }
}
