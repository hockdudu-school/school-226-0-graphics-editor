package ch.hockdudu.graphics_editor.editor.mode;

import ch.hockdudu.graphics_editor.display.Canvas;
import ch.hockdudu.graphics_editor.editor.Editor;
import ch.hockdudu.graphics_editor.component.Component;
import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.component.Group;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Selection extends BaseMode {

    @SuppressWarnings("WeakerAccess")
    protected Point clickPosition;
    @SuppressWarnings("WeakerAccess")
    protected List<Form> formsToApplyTransform = new ArrayList<>();

    public Selection(Editor editor, Canvas canvas, Group forms) {
        super(editor, canvas, forms);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        List<Form> reversedForms = reversedFormsList();
        List<Form> clickedForms = clickedForms(mouseEvent.getPoint());

        boolean isCtrlPressed = (mouseEvent.getModifiersEx() & MouseEvent.CTRL_DOWN_MASK) != 0;

        if (!isCtrlPressed) {
            // Deselect the selected ones that aren't on the mouse click
            reversedForms.stream().filter(form1 -> form1.isSelected() && !clickedForms.contains(form1)).forEach(form -> form.setSelected(false));
        }

        if (clickedForms.size() == 1) {
            clickedForms.get(0).setSelected(!clickedForms.get(0).isSelected());
        } else if (clickedForms(mouseEvent.getPoint()).size() > 1) {
            // Selection should loop as following (example with three items)
            // Select 1, select 2, select 3, select none
            if (clickedForms.stream().noneMatch(Component::isSelected)) {
                clickedForms.get(0).setSelected(true);
            } else {
                // TODO: Handle Ctrl press correctly
                boolean selectNext = false;
                for (Form clickedForm : clickedForms) {
                    if (selectNext) {
                        clickedForm.setSelected(true);
                        break;
                    }

                    if (clickedForm.isSelected()) {
                        selectNext = true;
                        clickedForm.setSelected(false);
                    }
                }
            }
        }

        canvas.repaint();
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        List<Form> clickedForms = clickedForms(mouseEvent.getPoint());

        boolean anySelected = forms.getFlatForms().stream().anyMatch(Component::isSelected);

        if (anySelected || clickedForms.size() > 0) {
            if (anySelected) {
                formsToApplyTransform = Arrays.asList(forms.getFlatForms().stream().filter(Component::isSelected).toArray(Form[]::new));
            } else { // Else if clickedForms.size() > 0
                formsToApplyTransform = clickedForms.subList(0, 1);
            }
            clickPosition = mouseEvent.getPoint();
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        clickPosition = null;
        formsToApplyTransform = null;
        canvas.setCursor(Cursor.getDefaultCursor());
    }


    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

    }
}
