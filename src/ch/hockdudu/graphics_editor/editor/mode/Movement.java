package ch.hockdudu.graphics_editor.editor.mode;

import ch.hockdudu.graphics_editor.display.Canvas;
import ch.hockdudu.graphics_editor.editor.Editor;
import ch.hockdudu.graphics_editor.component.Group;

import java.awt.*;
import java.awt.event.MouseEvent;

public class Movement extends Selection {
    public Movement(Editor editor, Canvas canvas, Group forms) {
        super(editor, canvas, forms);
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        if (clickPosition == null) return;

        double xDiff = mouseEvent.getX() - clickPosition.getX();
        double yDiff = mouseEvent.getY() - clickPosition.getY();

        formsToApplyTransform.forEach(form -> form.translate(xDiff, yDiff));
        canvas.repaint();

        canvas.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));

        clickPosition = mouseEvent.getPoint();
    }
}
