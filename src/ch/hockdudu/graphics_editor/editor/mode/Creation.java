package ch.hockdudu.graphics_editor.editor.mode;

import ch.hockdudu.graphics_editor.component.FormFactory;
import ch.hockdudu.graphics_editor.display.Canvas;
import ch.hockdudu.graphics_editor.editor.Editor;
import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.component.Group;
import ch.hockdudu.graphics_editor.loader.LoaderInterface;

import java.awt.*;
import java.awt.event.MouseEvent;

public class Creation extends BaseMode {

    private Form currentCreationForm;
    private Point initialMousePosition;
    private LoaderInterface loader;

    public Creation(Editor editor, Canvas canvas, Group forms, LoaderInterface loader) {
        super(editor, canvas, forms);
        this.loader = loader;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        initialMousePosition = mouseEvent.getPoint();
        getCurrentCreationForm().handleMouseAction(mouseEvent.getX(), mouseEvent.getY(), mouseEvent.getX(), mouseEvent.getY());

        canvas.add(getCurrentCreationForm());
        canvas.repaint();
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        currentCreationForm = null;
        initialMousePosition = null;
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        if (initialMousePosition != null) {
            getCurrentCreationForm().handleMouseAction(initialMousePosition.getX(), initialMousePosition.getY(), mouseEvent.getX(), mouseEvent.getY());
            canvas.repaint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

    }

    private Form getCurrentCreationForm() {
        if (currentCreationForm == null) {
            FormFactory formFactory = loader.getFormFactoryFromClass(editor.getChosenForm());
            if (formFactory != null) {
                currentCreationForm = formFactory.createForm();
            }
        }

        return currentCreationForm;
    }
}
