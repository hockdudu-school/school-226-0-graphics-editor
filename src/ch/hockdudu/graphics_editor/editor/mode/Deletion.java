package ch.hockdudu.graphics_editor.editor.mode;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.component.Group;
import ch.hockdudu.graphics_editor.display.Canvas;
import ch.hockdudu.graphics_editor.editor.Editor;

import java.awt.event.MouseEvent;
import java.util.List;

public class Deletion extends BaseMode {

    public Deletion(Editor editor, Canvas canvas, Group forms) {
        super(editor, canvas, forms);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        List<Form> clickedForms = clickedForms(mouseEvent.getPoint());
        if (clickedForms.size() > 0) {
            forms.remove(clickedForms.get(0));
            canvas.repaint();
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

    }
}
