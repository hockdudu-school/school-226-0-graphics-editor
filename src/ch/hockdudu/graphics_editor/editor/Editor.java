package ch.hockdudu.graphics_editor.editor;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.display.Canvas;
import ch.hockdudu.graphics_editor.display.InfobarInterface;
import ch.hockdudu.graphics_editor.editor.mode.*;
import ch.hockdudu.graphics_editor.loader.LoaderInterface;

import java.awt.event.*;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Editor implements ActionListener {

    public enum Mode {
        movement("Movement mode"),
        creation("Creation mode"),
        rotation("Rotation mode"),
        deletion("Deletion mode");

        private String text;

        Mode(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    private Class<? extends Form> chosenForm;

    private Mode mode = Mode.movement;

    private Canvas canvas;
    private InfobarInterface infobar;
    private LoaderInterface loader;
    private Map<Mode, BaseMode> modes;

    public Editor(Canvas canvas, InfobarInterface infobar, LoaderInterface loader) {
        this.canvas = canvas;
        this.infobar = infobar;
        this.loader = loader;
        this.modes = Map.ofEntries(
                Map.entry(Mode.movement, new Movement(this, canvas, canvas.getForms())),
                Map.entry(Mode.creation, new Creation(this, canvas, canvas.getForms(), loader)),
                Map.entry(Mode.rotation, new Rotation(this, canvas, canvas.getForms())),
                Map.entry(Mode.deletion, new Deletion(this, canvas, canvas.getForms()))
        );

        setChosenForm(loader.getForms().get(0));

        canvas.addMouseMotionListener(new MouseCoordsListener(infobar));

        resetModeListener(null, Mode.movement);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) throws IllegalArgumentException {
        Pattern formPattern = Pattern.compile("form\\.(?<formName>.+)");
        Pattern modePattern = Pattern.compile("mode\\.(?<modeName>.+)");

        Matcher formMatcher = formPattern.matcher(actionEvent.getActionCommand());
        Matcher modeMatcher = modePattern.matcher(actionEvent.getActionCommand());

        if (formMatcher.matches()) {
            String formName = formMatcher.group("formName");

            Class<? extends Form> form = loader.formClassFromName(formName);

            if (form == null) {
                throw new IllegalArgumentException(String.format("Form %s couldn't be found", formName));
            }
            setChosenForm(loader.formClassFromName(formName));
        } else if (modeMatcher.matches()) {
            String modeName = modeMatcher.group("modeName");

            Mode action = Mode.valueOf(modeName);
            resetModeListener(this.mode, action);
        } else {
            throw new IllegalArgumentException(String.format("Unknown action %s", actionEvent.getActionCommand()));
        }
    }

    private void resetModeListener(Mode previousListener, Mode newMode) {
        if (previousListener != null) {
            canvas.removeMouseListener(modes.get(previousListener));
            canvas.removeMouseMotionListener(modes.get(previousListener));
        }

        canvas.addMouseListener(modes.get(newMode));
        canvas.addMouseMotionListener(modes.get(newMode));

        setMode(newMode);
    }

    public Mode getMode() {
        return mode;
    }

    private void setMode(Mode mode) {
        this.mode = mode;
        infobar.updateMode(mode);
    }

    public Class<? extends Form> getChosenForm() {
        return chosenForm;
    }

    private void setChosenForm(Class<? extends Form> chosenForm) {
        this.chosenForm = chosenForm;
        infobar.updateForm(chosenForm);
    }
}
