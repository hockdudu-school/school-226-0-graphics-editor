package ch.hockdudu.graphics_editor.editor;

import ch.hockdudu.graphics_editor.display.InfobarInterface;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class MouseCoordsListener implements MouseMotionListener {

    private InfobarInterface infobar;

    public MouseCoordsListener(InfobarInterface infobar) {
        this.infobar = infobar;
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        infobar.updateMouseCoords(mouseEvent.getPoint());
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        infobar.updateMouseCoords(mouseEvent.getPoint());

    }
}
