package ch.hockdudu.graphics_editor.component.forms;

public class Circle extends Oval {

    public Circle(double x, double y, double r) {
        super(x, y, r, r);
    }

    public double getR() {
        return getRw();
    }

    public void setR(double r) {
        setRw(r);
        setRh(r);
    }

    @Override
    public void handleMouseAction(double initialX, double initialY, double currentX, double currentY) {

        double horizontalDistance = Math.abs(initialX - currentX);
        double verticalDistance = Math.abs(initialY - currentY);

        double radius = Math.sqrt(Math.pow(horizontalDistance, 2) + Math.pow(verticalDistance, 2));

        this.x = initialX;
        this.y = initialY;
        this.setR(radius);
    }
}
