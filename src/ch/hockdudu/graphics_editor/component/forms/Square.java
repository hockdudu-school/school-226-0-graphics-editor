package ch.hockdudu.graphics_editor.component.forms;

public class Square extends Rectangle {

    public Square(double x, double y, double size) {
        super(x, y, size, size);
    }

    @Override
    public void setWidth(double width) {
        //noinspection SuspiciousNameCombination
        super.setHeight(width);
        super.setWidth(width);
    }

    @Override
    public void setHeight(double height) {
        //noinspection SuspiciousNameCombination
        this.setWidth(height);
    }

    @Override
    public void handleMouseAction(double initialX, double initialY, double currentX, double currentY) {
        double width = Math.abs(currentX - initialX);
        double height = Math.abs(currentY - initialY);

        double side = Math.max(width, height);

        if (initialX > currentX) {
            currentX = initialX - side;
        } else {
            currentX = initialX + side;
        }

        if (initialY > currentY) {
            currentY = initialY - side;
        } else {
            currentY = initialY + side;
        }

        super.handleMouseAction(initialX, initialY, currentX, currentY);
    }
}
