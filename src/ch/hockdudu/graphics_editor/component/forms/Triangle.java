package ch.hockdudu.graphics_editor.component.forms;

import ch.hockdudu.graphics_editor.component.Form;

import java.awt.*;
import java.awt.geom.Path2D;

public class Triangle extends Form {

    private double sideSize;

    public Triangle(double x, double y, double sideSize) {
        super(x, y);
        this.sideSize = sideSize;
    }

    @Override
    public Shape getShape() {
        double height = Math.cos(Math.PI / 6) * sideSize; // 30 degrees in radians

        double heightDifference = - height * 2 / 3; // Middle is on 2 / 3 from above

        Path2D path2D = new Path2D.Double();

        double firstPointX = x;
        double firstPointY = y + heightDifference;

        double secondPointX = x - sideSize / 2;
        double secondPointY = y + height + heightDifference;

        double thirdPointX = x + sideSize / 2;
        double thirdPointY = y + height + heightDifference;


        path2D.moveTo(firstPointX, firstPointY);
        path2D.lineTo(secondPointX, secondPointY);
        path2D.lineTo(thirdPointX, thirdPointY);
        path2D.closePath();

        return path2D;
    }

    @Override
    public void handleMouseAction(double initialX, double initialY, double currentX, double currentY) {
        this.x = initialX;
        this.y = initialY;

        double horizontalDifference = currentX - initialX;
        double verticalDifference = currentY - initialY;

        // cos 30 * sqrt(hDiff^2 + vDiff^2) * 2
        this.sideSize = Math.cos(Math.PI / 6) * Math.sqrt(Math.pow(horizontalDifference, 2) + Math.pow(verticalDifference, 2)) * 2;

        // Adds 90 degrees for resetting rotation
        double radians = Math.atan2(verticalDifference, horizontalDifference) + Math.PI / 2;

        this.rotate(radians);
    }
}
