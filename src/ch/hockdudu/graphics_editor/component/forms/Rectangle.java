package ch.hockdudu.graphics_editor.component.forms;

import ch.hockdudu.graphics_editor.component.Form;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Rectangle extends Form {

    private double width;
    private double height;

    public Rectangle(double x, double y, double width, double height) {
        super(x, y);
        this.width = width;
        this.height = height;
    }

    @Override
    public Shape getShape() {
        double relativeX = x - width/2;
        double relativeY = y - height/2;
        return new Rectangle2D.Double(relativeX, relativeY, width, height);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public void handleMouseAction(double initialX, double initialY, double currentX, double currentY) {
        double width = currentX - initialX;
        double height = currentY - initialY;

        double realX = initialX + width / 2;
        double realY = initialY + height / 2;

        this.x = realX;
        this.y = realY;
        this.width = Math.abs(width);
        this.height = Math.abs(height);
    }
}
