package ch.hockdudu.graphics_editor.component.forms;

import ch.hockdudu.graphics_editor.component.Form;

import java.awt.*;
import java.awt.geom.Path2D;

public class Star extends Form {

    private double size;
    private double innerSizeRatio;

    public Star(double x, double y, double size) {
        this(x, y, size, Math.sin(Math.PI * 0.1) / Math.sin(Math.PI * 0.3));
    }

    @SuppressWarnings("WeakerAccess")
    public Star(double x, double y, double size, double innerSizeRatio) {
        super(x, y);
        this.size = size;
        this.innerSizeRatio = innerSizeRatio;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    @Override
    public Shape getShape() {
        Path2D star = new Path2D.Double();
        double innerSize = size * innerSizeRatio;

        // Pentagon has 72 for every angle (360/5). In radians, it's pi / 2.5

        // First angle is 90 - 72 / 2 -> 54 -> pi * 0.3
        double firstPentagonEdgeX = Math.cos(Math.PI * 0.3) * innerSize;
        double firstPentagonEdgeY = Math.sin(Math.PI * 0.3) * innerSize;

        // Second angle is 54 - 72 -> -18 -> pi * -0.1
        double secondPentagonEdgeX = Math.cos(Math.PI * -0.1) * innerSize;
        double secondPentagonEdgeY = Math.sin(Math.PI * -0.1) * innerSize;

        double thirdPentagonEdgeX = 0;
        double thirdPentagonEdgeY = -innerSize;

        double fourthPentagonEdgeX = -secondPentagonEdgeX;
        double fourthPentagonEdgeY = secondPentagonEdgeY;

        double fifthPentagonEdgeX = -firstPentagonEdgeX;
        double fifthPentagonEdgeY = firstPentagonEdgeY;

        // Now for the edges from the star
        // Same as above, but inverted, and in a different order

        double firstStarEdgeX = 0;
        double firstStarEdgeY = size;

        double secondStarEdgeX = Math.cos(Math.PI * 0.1) * size;
        double secondStarEdgeY = Math.sin(Math.PI * 0.1) * size;

        double thirdStarEdgeX = Math.cos(Math.PI * -0.3) * size;
        double thirdStarEdgeY = Math.sin(Math.PI * -0.3) * size;

        double fourthStarEdgeX = -thirdStarEdgeX;
        double fourthStarEdgeY = thirdStarEdgeY;

        double fifthStarEdgeX = -secondStarEdgeX;
        double fifthStarEdgeY = secondStarEdgeY;

        star.moveTo(x + firstStarEdgeX, y - firstStarEdgeY);
        star.lineTo(x + firstPentagonEdgeX, y - firstPentagonEdgeY);
        star.lineTo(x + secondStarEdgeX, y - secondStarEdgeY);
        star.lineTo(x + secondPentagonEdgeX, y - secondPentagonEdgeY);
        star.lineTo(x + thirdStarEdgeX, y - thirdStarEdgeY);
        star.lineTo(x + thirdPentagonEdgeX, y - thirdPentagonEdgeY);
        star.lineTo(x + fourthStarEdgeX, y - fourthStarEdgeY);
        star.lineTo(x + fourthPentagonEdgeX, y - fourthPentagonEdgeY);
        star.lineTo(x + fifthStarEdgeX, y - fifthStarEdgeY);
        star.lineTo(x + fifthPentagonEdgeX, y - fifthPentagonEdgeY);
        star.closePath();

        return star;
    }

    @Override
    public void handleMouseAction(double initialX, double initialY, double currentX, double currentY) {
        this.x = initialX;
        this.y = initialY;

        double horizontalDifference = currentX - initialX;
        double verticalDifference = currentY - initialY;

        this.size =  Math.sqrt(Math.pow(horizontalDifference, 2) + Math.pow(verticalDifference, 2));

        // Adds 90 degrees for resetting rotation
        double radians = Math.atan2(verticalDifference, horizontalDifference) + Math.PI / 2;

        this.rotate(radians);
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getInnerSizeRatio() {
        return innerSizeRatio;
    }

    public void setInnerSizeRatio(double innerSizeRatio) {
        this.innerSizeRatio = innerSizeRatio;
    }
}
