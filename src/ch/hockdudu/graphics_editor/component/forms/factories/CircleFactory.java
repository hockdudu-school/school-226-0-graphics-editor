package ch.hockdudu.graphics_editor.component.forms.factories;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.component.FormFactory;
import ch.hockdudu.graphics_editor.component.forms.Circle;

public class CircleFactory extends FormFactory {
    @Override
    public Form createForm() {
        return new Circle(0, 0, 0);
    }
}
