package ch.hockdudu.graphics_editor.component.forms.factories;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.component.FormFactory;
import ch.hockdudu.graphics_editor.component.forms.Oval;

public class OvalFactory extends FormFactory {
    @Override
    public Form createForm() {
        return new Oval(0, 0, 0, 0);
    }
}
