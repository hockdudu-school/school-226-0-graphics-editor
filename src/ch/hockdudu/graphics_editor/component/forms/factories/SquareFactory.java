package ch.hockdudu.graphics_editor.component.forms.factories;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.component.FormFactory;
import ch.hockdudu.graphics_editor.component.forms.Square;

public class SquareFactory extends FormFactory {
    @Override
    public Form createForm() {
        return new Square(0, 0, 0);
    }
}
