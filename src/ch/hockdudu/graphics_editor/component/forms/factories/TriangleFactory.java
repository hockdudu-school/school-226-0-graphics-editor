package ch.hockdudu.graphics_editor.component.forms.factories;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.component.FormFactory;
import ch.hockdudu.graphics_editor.component.forms.Triangle;

public class TriangleFactory extends FormFactory {
    @Override
    public Form createForm() {
        return new Triangle(0, 0, 0);
    }
}
