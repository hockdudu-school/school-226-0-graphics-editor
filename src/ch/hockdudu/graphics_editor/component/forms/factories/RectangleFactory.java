package ch.hockdudu.graphics_editor.component.forms.factories;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.component.FormFactory;
import ch.hockdudu.graphics_editor.component.forms.Rectangle;

public class RectangleFactory extends FormFactory {
    @Override
    public Form createForm() {
        return new Rectangle(0, 0, 0, 0);
    }
}
