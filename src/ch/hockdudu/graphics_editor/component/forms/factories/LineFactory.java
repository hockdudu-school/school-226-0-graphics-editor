package ch.hockdudu.graphics_editor.component.forms.factories;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.component.FormFactory;
import ch.hockdudu.graphics_editor.component.forms.Line;

public class LineFactory extends FormFactory {
    @Override
    public Form createForm() {
        return new Line(0, 0, 0);
    }
}
