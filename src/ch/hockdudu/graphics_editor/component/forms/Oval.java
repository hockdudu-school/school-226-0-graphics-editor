package ch.hockdudu.graphics_editor.component.forms;

import ch.hockdudu.graphics_editor.component.Form;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Oval extends Form {

    private double rw;
    private double rh;

    public Oval(double x, double y, double rw, double rh) {
        super(x, y);
        this.rw = rw;
        this.rh = rh;
    }

    @Override
    public Shape getShape() {
        double realX = x - rw;
        double realY = y - rh;
        return new Ellipse2D.Double(realX, realY, rw * 2, rh * 2);
    }

    public double getRw() {
        return rw;
    }

    public void setRw(double rw) {
        this.rw = rw;
    }

    public double getRh() {
        return rh;
    }

    public void setRh(double rh) {
        this.rh = rh;
    }

    @Override
    public void handleMouseAction(double initialX, double initialY, double currentX, double currentY) {
        double width = currentX - initialX;
        double height = currentY - initialY;

        double realX = initialX + width / 2;
        double realY = initialY + height / 2;

        this.x = realX;
        this.y = realY;
        this.rw = Math.abs(width) / 2;
        this.rh = Math.abs(height) / 2;
    }
}
