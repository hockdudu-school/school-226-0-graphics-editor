package ch.hockdudu.graphics_editor.component.forms;

import ch.hockdudu.graphics_editor.component.Form;

import java.awt.*;
import java.awt.geom.Line2D;

public class Line extends Form {

    private double length;

    public Line(double x, double y, double length) {
        super(x, y);
        this.length = length;
    }

    @Override
    public Shape getShape() {
        double halfLength = length / 2;
       return new Line2D.Double(x - halfLength, y, x + halfLength, y);
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public void handleMouseAction(double initialX, double initialY, double currentX, double currentY) {
        double horizontalLength = currentX - initialX;
        double verticalLength = currentY - initialY;

        double realX = initialX + horizontalLength / 2;
        double realY = initialY + verticalLength / 2;

        double length = Math.sqrt(Math.pow(horizontalLength, 2) + Math.pow(verticalLength, 2));

        double rotation = Math.atan2(verticalLength, horizontalLength);

        this.x = realX;
        this.y = realY;
        this.length = length;

        this.rotate(rotation);
    }
}
