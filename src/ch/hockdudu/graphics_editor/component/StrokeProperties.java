package ch.hockdudu.graphics_editor.component;

import java.awt.*;
import java.io.Serializable;

public class StrokeProperties implements Serializable {
    private int width = 2;
    private Color color = Color.BLACK;
    private int cap = BasicStroke.CAP_BUTT;
    private int join = BasicStroke.JOIN_BEVEL;
    private int miterLimit = 0;
    private float[] dash = null;
    private int dashPhase = 1;

    private transient Stroke cachedStroke = null;

    StrokeProperties() {
    }

    StrokeProperties(Color color, float[] dash) {
        this.color = color;
        this.dash = dash;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
        cachedStroke = null;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
        cachedStroke = null;
    }

    public int getCap() {
        return cap;
    }

    public void setCap(int cap) {
        this.cap = cap;
        cachedStroke = null;
    }

    public int getJoin() {
        return join;
    }

    public void setJoin(int join) {
        this.join = join;
        cachedStroke = null;
    }

    public int getMiterLimit() {
        return miterLimit;
    }

    public void setMiterLimit(int miterLimit) {
        this.miterLimit = miterLimit;
        cachedStroke = null;
    }

    public float[] getDash() {
        return dash;
    }

    public void setDash(float[] dash) {
        this.dash = dash;
        cachedStroke = null;
    }

    public int getDashPhase() {
        return dashPhase;
    }

    public void setDashPhase(int dashPhase) {
        this.dashPhase = dashPhase;
        cachedStroke = null;
    }

    @SuppressWarnings("WeakerAccess")
    public Stroke getStroke() {
        if (cachedStroke == null) cachedStroke = new BasicStroke(width, cap, join, miterLimit, dash, dashPhase);
        return cachedStroke;

    }
}