package ch.hockdudu.graphics_editor.component;

import java.awt.*;
import java.io.Serializable;

public abstract class Component implements Serializable {
    @SuppressWarnings("WeakerAccess")
    protected boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public abstract void draw(Graphics2D graphics2D);
    public abstract void translate(double deltaX, double deltaY);
    public abstract void rotate(double radians);
}
