package ch.hockdudu.graphics_editor.component;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Group extends Component {
    private List<Component> forms = new ArrayList<>();

    public void add(Component... forms) {
        this.forms.addAll(Arrays.asList(forms));
    }

    public void add(List<Component> forms) {
        this.forms.addAll(forms);
    }

    public boolean remove(Component... forms) {
        return this.forms.removeAll(Arrays.asList(forms));
    }

    public boolean remove(List<Component> forms) {
        return this.forms.removeAll(forms);
    }

    public void clear() {
        this.forms.clear();
    }

    public List<Component> getForms() {
        return forms;
    }

    public void setForms(List<Component> forms) {
        this.forms = forms;
    }

    public List<Form> getFlatForms() {
        List<Form> forms = new ArrayList<>();

        for (Component form : this.forms) {
            if (form instanceof Form) {
                forms.add((Form) form);
            } else if (form instanceof Group) {
                forms.addAll(((Group) form).getFlatForms());
            }
        }

        return forms;
    }

    @Override
    public void draw(Graphics2D graphics2D) {
        for (Component form : forms) {
            form.draw(graphics2D);
        }
    }

    @Override
    public void translate(double deltaX, double deltaY) {
        for (Component form : forms) {
            form.translate(deltaX, deltaY);
        }
    }

    @Override
    public void rotate(double radians) {
        // TODO: Implement rotation
    }
}
