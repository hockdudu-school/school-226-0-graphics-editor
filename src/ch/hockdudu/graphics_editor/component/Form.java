package ch.hockdudu.graphics_editor.component;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;

@SuppressWarnings("WeakerAccess")
public abstract class Form extends Component {
    protected double x;
    protected double y;
    protected StrokeProperties stroke = new StrokeProperties();
    protected StrokeProperties selectionStroke = new StrokeProperties(new Color(0xff, 0x22, 0x00, 0xdd), new float[] {16, 4, 2, 4});
    protected Color fillColor = null;
    protected double rotation;

    protected Form(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void draw(Graphics2D graphics2D) {
        Shape shape = getRotatedShape();

        graphics2D.setStroke(stroke.getStroke());

        if (fillColor != null) {
            graphics2D.setColor(fillColor);
            graphics2D.fill(shape);
        }

        if (stroke.getColor() != null) {
            graphics2D.setColor(stroke.getColor());
            graphics2D.draw(shape);
        }

        if (this.isSelected) {
            graphics2D.setColor(selectionStroke.getColor());
            graphics2D.setStroke(selectionStroke.getStroke());
            graphics2D.draw(shape.getBounds2D());
        }
    }

    public Shape getRotatedShape() {
        Shape shape = getShape();

        AffineTransform transform = new AffineTransform();
        transform.rotate(rotation, x, y);
        return new Path2D.Double(shape, transform);
    }

    public abstract Shape getShape();

    public abstract void handleMouseAction(double initialX, double initialY, double currentX, double currentY);

    @Override
    public void translate(double deltaX, double deltaY) {
        this.x += deltaX;
        this.y += deltaY;
    }

    @Override
    public void rotate(double radians) {
        setRotation(radians);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public StrokeProperties getStroke() {
        return stroke;
    }

    public void setStroke(StrokeProperties stroke) {
        this.stroke = stroke;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    public double getRotation() {
        return rotation;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }
}
