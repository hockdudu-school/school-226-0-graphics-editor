package ch.hockdudu.graphics_editor.component;

public abstract class FormFactory {
    public abstract Form createForm();
}
