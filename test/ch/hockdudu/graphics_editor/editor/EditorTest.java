package ch.hockdudu.graphics_editor.editor;

import ch.hockdudu.graphics_editor.component.Form;
import ch.hockdudu.graphics_editor.component.FormFactory;
import ch.hockdudu.graphics_editor.component.forms.Circle;
import ch.hockdudu.graphics_editor.component.forms.Square;
import ch.hockdudu.graphics_editor.component.forms.Triangle;
import ch.hockdudu.graphics_editor.display.Canvas;
import ch.hockdudu.graphics_editor.display.InfobarInterface;
import ch.hockdudu.graphics_editor.loader.LoaderInterface;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class EditorTest {

    private LoaderInterface loader;

    @BeforeAll
    void prepareLoader() {
        loader = new LoaderInterface() {
            @Override
            public List<Class<? extends Form>> getForms() {
                var forms = new ArrayList<Class<? extends Form>>();
                forms.add(Circle.class);
                forms.add(Square.class);
                forms.add(Triangle.class);

                return forms;
            }

            @Override
            public FormFactory getFormFactoryFromClass(Class<? extends Form> class1) {
                return null;
            }

            @Override
            public Class<? extends Form> formClassFromName(String className) {
                switch (className) {
                    case "Circle":
                        return Circle.class;
                    case "Square":
                        return Square.class;
                    case "Triangle":
                        return Triangle.class;
                    default:
                        return null;
                }
            }
        };
    }


    /**
     * The Editor should use the first form from the loader as the default
     * On an ActionEvent of type form.xyz, it should call the correct form class
     */
    @Test
    void actionPerformedForm() {
        var editor = new Editor(new Canvas(), new InfobarInterface() {
            private int count = 0;

            @Override
            public void updateMode(Editor.Mode mode) {}

            @Override
            public void updateForm(Class<? extends Form> form) {
                switch (count) {
                    case 0:
                        assertEquals(Circle.class, form);
                        break;
                    case 1:
                        assertEquals(Square.class, form);
                        break;
                    case 2:
                        assertEquals(Circle.class, form);
                        break;
                    case 3:
                        assertEquals(Circle.class, form);
                        break;
                    case 4:
                        assertEquals(Triangle.class, form);
                        break;
                    default:
                        fail("Too many updateForm calls");
                }

                count++;
            }

            @Override
            public void updateMouseCoords(Point mouseCoords) {}
        }, loader);

        editor.actionPerformed(new ActionEvent(this, 0, "form.Square"));
        editor.actionPerformed(new ActionEvent(this, 0, "form.Circle"));
        editor.actionPerformed(new ActionEvent(this, 0, "form.Circle"));
        editor.actionPerformed(new ActionEvent(this, 0, "form.Triangle"));
        assertThrows(IllegalArgumentException.class, () -> editor.actionPerformed(new ActionEvent(this, 0, "form.unexistent")));
    }

    /**
     * The editor should default the mode to movement mode
     * On an ActionEvent of type mode.xzy, it should call the correct movement mode
     */
    @Test
    void actionPerformedMode() {
        var editor = new Editor(new Canvas(), new InfobarInterface() {
            private int count = 0;

            @Override
            public void updateMode(Editor.Mode mode) {
                switch (count) {
                    case 0:
                        assertEquals(Editor.Mode.movement, mode);
                        break;
                    case 1:
                        assertEquals(Editor.Mode.creation, mode);
                        break;
                    case 2:
                        assertEquals(Editor.Mode.movement, mode);
                        break;
                    case 3:
                        assertEquals(Editor.Mode.rotation, mode);
                        break;
                    default:
                        fail("Too many updateMode calls");
                }

                count++;
            }

            @Override
            public void updateForm(Class<? extends Form> form) {}

            @Override
            public void updateMouseCoords(Point mouseCoords) {}
        }, loader);

        editor.actionPerformed(new ActionEvent(this, 0, "mode.creation"));
        editor.actionPerformed(new ActionEvent(this, 0, "mode.movement"));
        editor.actionPerformed(new ActionEvent(this, 0, "mode.rotation"));
        assertThrows(IllegalArgumentException.class, () -> editor.actionPerformed(new ActionEvent(this, 0, "mode.unexistent")));
    }

    @Test
    void invalidActionMode() {
        var editor = new Editor(new Canvas(), new InfobarInterface() {
            @Override
            public void updateMode(Editor.Mode mode) {}

            @Override
            public void updateForm(Class<? extends Form> form) {}

            @Override
            public void updateMouseCoords(Point mouseCoords) {}
        }, loader);

        assertThrows(IllegalArgumentException.class, () -> editor.actionPerformed(new ActionEvent(this, 0, "unexistent.action")));
    }
}